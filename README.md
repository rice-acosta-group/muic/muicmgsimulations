# Running MuICMGStudies

# Requirements

* pythia8
* Delphes
* MadAnalysis
* lhapdf6

```bash
yay -S madgraph
sudo mg5_aMC
install pythia8
install Delphes
install MadAnalysis5
install lhapdf6
```

## Getting the PDF for the studies

```bash
lhapdf install PDF4LHC15_nlo_mc_pdfas
```

## Running on Osvaldo's machine
For some reason my installation of madgraph didn't want to install the lhapdf binaries,
hence I did the following to get it to work properly.

Notice the symlink at the bottom, I had to create this since madgraph kept looking for the same library but with extension '.a', so a quick workaround was to 'alias' it.

After doing these changes I was able to work normally.

```bash
sudo pacman -S lhapdf Delphes
yay -S madgraph
sudo mg5_aMC
install pythia8
install MadAnalysis5
exit

sudo ln -s /usr/lib/libLHAPDF.so /usr/lib/libLHAPDF.a
```

# How to Run

1. Clone repository

```bash
git clone git@gitlab.com:omiguelc/muicmgsimulations.git
```

2. Setup Magraph

This will create a madgraph directory containing all the scripts required to run the studies in the working directory.

```bash
cd muicmgsimulations
bin/run --setup
```

3. Set the environment variables

If you have another version of pythia on the same machine, you need to tell it to use the one that was installed with madgraph, otherwise you might get a version mismatch.

```bash
export PYTHIA8=<path to madgraph>/HEPTools/pythia8
export PYTHIA8DATA=$PYTHIA8/share/Pythia8/xmldoc

# Example
# Note the path may be different depending on how you installed madgraph
export PYTHIA8=/opt/madgraph/HEPTools/pythia8
export PYTHIA8DATA=$PYTHIA8/share/Pythia8/xmldoc
```

4. Generate Processes

Executes all the process generators in scripts/processes

```bash
# Enter newly generated madgraph env
cd madgraph
# Create an output directory
mkdir out
# Enter output directory
cd out
# Generate processes
../bin/exec_all_process_generators
```

5. Run all run scripts

Executes all the run scripts in scripts/runs

```bash
# Execute all runs
# Note: All processes must have been generated for this command to work.
../bin/exec_all_runs
```

6. Collect all the mg5 results

This will output 2 files in the working directory.
    1. xsec.csv: Contains all the xsec values and the errors.
    2. sys.csv: Contains all the systematic errors.

```bash
../bin/collect_mg5_results
```

7. Run mg5 lhe events on pythia

The setup process should have generated a folder called pythia_runs. This folder contains scripts for running pythia on the mg5 lhe events.

```bash
<process folder>/bin/madevent ../pythia_runs/<script>.txt

# Example
nc/bin/madevent ../pythia_runs/run_pythia_MuIC2_nc.txt
```

8. Run delphes on pythia hepmc files

Using a local install of delphes and the MuIC.tcl card provided in the setup step.
It's worth noting that you can use madgraph's delphes instead, look at madgraph documentation for this.

```bash
# Copy events
cp nc/Events/<run>/<filename>.hepmc.gz .

# Unzip events
gunzip <filename>.hepmc.gz

# Make Delphes go brrrrrr
DelphesHepMC2 ../cards/MuIC.tcl <run>.root <filename>.hepmc
```