# Z'
v_phi = 2500


def calc_g_mu(mz):
    return mz / v_phi


def calc_g_b(delbs, g_mu, mz):
    return 1.3e-9 * mz ** 2 / (delbs * g_mu)
