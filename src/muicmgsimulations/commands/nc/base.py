model_commands = {
    '5-sm': [
        'import model sm-no_b_mass'
    ]
}

base_config = {
    'pdlabel': 'lhapdf',
    'lhaid': 90000,
    'nevents': 50000,
    'lpp1': 0,
    'lpp2': 0,
    'ebeam1': 0,
    'ebeam2': 0,
    'polbeam1': 0,
    'polbeam2': 0,
    # DISABLE SYSTEMATICS
    'use_syst': False,
    # DISABLE FUDICIAL CUTS
    'ptj': 0,
    'ptl': 0,
    'ptjmax': -1,
    'ptlmax': -1,
    'pt_min_pdg': '{}',
    'pt_max_pdg': '{}',
    'etaj': -1,
    'etal': -1,
    'etalmin': 0,
    'eta_min_pdg': '{}',
    'eta_max_pdg': '{}',
    'drjl': 0,
    'drjlmax': -1,
    'ptheavy': 0,
    'maxjetflavor': 5
}
