import os
import shutil

from muicmgsimulations.commands.nc import simulations
from muicmgsimulations.execution import runtime


def configure_parser(subparser):
    pass


def run(args):
    print('Generating Madgraph Diretory')
    os.makedirs(runtime.output_dir, exist_ok=True)

    print('Copying helper scripts')
    bin_dir = os.path.join(runtime.output_dir, 'bin')
    os.makedirs(bin_dir, exist_ok=True)
    shutil.copy(runtime.resource('madgraph/bin/exec_all_process_generators'), bin_dir)
    shutil.copy(runtime.resource('madgraph/bin/exec_all_runs'), bin_dir)
    shutil.copy(runtime.resource('madgraph/bin/collect_mg5_results'), bin_dir)

    print('Copying patches')
    patches_dir = os.path.join(runtime.output_dir, 'patches')
    os.makedirs(patches_dir, exist_ok=True)
    shutil.copy(runtime.resource('madgraph/patches/cuts.f'), patches_dir)
    shutil.copy(runtime.resource('madgraph/patches/cuts_range.f'), patches_dir)
    shutil.copy(runtime.resource('madgraph/patches/setscales.f'), patches_dir)

    print('Copying cards')
    cards_dir = os.path.join(runtime.output_dir, 'cards')
    os.makedirs(cards_dir, exist_ok=True)
    shutil.copy(runtime.resource('madgraph/cards/MuIC.tcl'), cards_dir)

    print('Generating process generation scripts')
    processes_dir = os.path.join(runtime.output_dir, 'processes')
    os.makedirs(processes_dir, exist_ok=True)
    simulations.generate_process_generators(processes_dir)

    print('Generating run scripts')
    runs_dir = os.path.join(runtime.output_dir, 'runs')
    os.makedirs(runs_dir, exist_ok=True)
    simulations.generate_long_runs(runs_dir)
