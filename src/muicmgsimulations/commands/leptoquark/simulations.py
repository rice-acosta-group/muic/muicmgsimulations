from muicmgsimulations.commands.leptoquark.base import model_commands, base_config
from muicmgsimulations.execution import runtime
from muicmgsimulations.utils.constants import machines

processes = [
    {
        'name': 'lq_b_mu_scat',
        'title': 'Pure Leptoquark b-mu scattering',
        'commands': ['define p = b', 'define j = b', 'generate p mu- > j mu- NP^2==4'],
        'model': 's3'
    },
    {
        'name': 'intf_b_mu_scat',
        'title': 'Leptoquark b-mu scattering',
        'commands': ['define p = b', 'define j = b', 'generate p mu- > j mu- QED^2<=2 NP^2<=2'],
        'model': 's3'
    },
    {
        'name': 'sm_b_mu_scat',
        'title': 'Pure SM b-mu scattering',
        'commands': ['define p = b', 'define j = b', 'generate p mu- > j mu- QED^2==4'],
        'model': 's3'
    },
]


def generate_process_generators(output_dir):
    # GENERATE PROCESSES
    for process in processes:
        print('Generating commands for %s' % process['title'])

        commands = list()

        # APPEND FLAVOR SUPPORT
        commands.extend(model_commands.get(process['model'], list()))

        # APPEND PROCESS
        commands.extend(process['commands'])

        # APPEND OUTPUT
        commands.append('output %s\n' % process['name'])

        # BUILD INPUT
        commands_output = ''

        for command in commands:
            commands_output += '%s%s' % ('' if commands_output == '' else '\n', command)

        with open('%s/generate_process_%s.txt' % (output_dir, process['name']), 'w') as f:
            f.write(commands_output)

        print('Done: %s\n' % process['name'])


def generate_mass_scan_runs(output_dir):
    process_list = ['lq_b_mu_scat', 'intf_b_mu_scat', 'sm_b_mu_scat']
    machine_list = ['MuIC', 'MuIC2', 'LHmuC']

    for machine_name in machine_list:
        machine = machines[machine_name]

        for process_name in process_list:
            masses = [400, 500, 600, 700, 800, 900, 1000, 1500, 3000]

            for mass in masses:
                run_name = '%s_mz_%sGeV' % (machine_name, mass)
                filename = 'mass_scan_%s_%s_mz_%sGeV' % (process_name, machine_name, mass)

                print('Generating %s' % filename)

                commands_output = 'set automatic_html_opening False\n'
                commands_output += 'launch %s -n %s\n0\n' % (process_name, run_name)

                config = base_config.copy()
                config['iseed'] = runtime.seeds.next()
                config['nevents'] = 10000
                config['lpp1'] = 1  # 0=NOPDF, 1=PROTON
                config['lpp2'] = 0  # 0=NOPDF, 1=PROTON
                config['ebeam1'] = machine['Ep']
                config['ebeam2'] = machine['Emu']
                config['polbeam1'] = 0
                config['polbeam2'] = 0
                config['dynamical_scale_choice'] = -1
                # LQ
                config['MS3'] = '%d' % mass  # GeV

                for key, value in config.items():
                    commands_output += 'set %s %s\n' % (key, value)

                commands_output += '0\n'

                with open('%s/run_%s.txt' % (output_dir, filename), 'w') as f:
                    f.write(commands_output)

                print('Done: %s\n' % filename)
